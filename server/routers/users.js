var express = require('express');
var router = express.Router();
var Users = require('../controllers/users');

router.get('/search/', Users.search);

router.get('/', Users.get);

router.get('/:username', Users.getOne);

router.post('/', Users.create);

router.put('/:username', Users.update);

router.get('/notingroup/:group_name', Users.getNotInGroup);

router.patch('/:username', Users.updateGroups);

router.delete('/:username', Users.delete);

module.exports = router;