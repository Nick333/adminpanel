var express = require('express');
var router = express.Router();
var Groups= require('../controllers/groups');

router.get('/search/', Groups.search);

router.get('/', Groups.get);

router.get('/:group_name', Groups.getOne);

router.post('/', Groups.create);

router.put('/:group_name', Groups.update);

router.delete('/:group_name', Groups.delete);

module.exports = router;

