var dao = require('../dao/mongodb/');
var async = require('async');


exports.create = function (req, res) {
    dao.create('users', {
        username: req.body.username,
        email: req.body.email,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        groups: []
    }, (err, result)=>{
        if(err){
            return res.status(500).send(err);
        }
        res.send(JSON.stringify(result))
    });
};

exports.updateGroups = function (req, res) {
    dao.update('users', {username:req.params.username}, {$addToSet: {groups:req.body.group_name}}, {}, (err)=>{
        if(err){
            return res.status(500).send(err);
        }
        dao.getOne('users', {username:req.params.username}, (err, userInfo)=>{
            if(err){
                return res.status(500).send(err);
            }
            delete userInfo.groups;
            res.send(JSON.stringify(userInfo));
        })
    })
};


exports.update = function (req, res) {
    async.waterfall([
        (next)=>{
            dao.update('users', {username:req.params.username}, {$set:req.body}, {}, next)
        },
        (next)=>{
            dao.getOne('users', {username:req.body.username}, (err, userInfo)=>{
                next(null, userInfo)
            })
        },
        (userInfo, next)=>{
            dao.get('groups', {group_name: {$in: userInfo.groups}}, {users: 0}, (err, groupsList)=>{
                userInfo.groups = groupsList || [];
                next(err, userInfo)
            })
        }
    ], (err, user)=>{
        if(err){
            return res.status(500).send(err);
        }
        res.send(JSON.stringify(user));
    });
};

exports.get = function (req, res) {
    var page = req.query.page || 0;
    var size = 5;
    var skip = (page > 0) ? ((page-1) * size) : 0;
    if(page != 0){
        dao.get('users', null, {skip:skip, limit:size},
            (err, result)=>{
                if(err){
                    return res.status(500).send(err);
                }
                res.send(JSON.stringify(result))
            });
    }else{
        dao.get('users', {}, {},
            (err, result)=>{
                if(err){
                    return res.status(404).send(err);
                }
                res.send(JSON.stringify(result.length))
            });
    }
};

exports.getOne = function (req, res) {
    var user = null;
    dao.getOne('users', {username:req.params.username},
        (err, userInfo)=>{
            if(err){
                return res.status(500).send(err);
            }
            else if(!userInfo){
                return res.status(404).send('User not found')
            }
            user = userInfo;
               dao.get('groups', {group_name: {$in: user.groups}}, {users: 0}, (err, groupsList)=>{
                   if(err){
                       return res.status(500).send(err);
                   }
                   user.groups = groupsList || [];
                   res.send(JSON.stringify(user));
               })
        })
};

exports.delete = function (req, res) {
    dao.delete('users', {username: req.params.username}, (err)=>{
        if(err){
            return res.status(500).send(err);
        }
        res.sendStatus(200);
    })
};

exports.getNotInGroup = function (req, res) {
        dao.getNotInGroup({groups: {$ne: req.params.group_name}}, (err, usersList)=>{
            if (err){
                return res.status(500).send(err);
            }else {
                res.send(JSON.stringify(usersList));
            }
        })
};

exports.search = function (req, res) {
    var crit = req.query.crit;
    var pattern = req.query.pattern;
    if(pattern != '^'){
        dao.search('users', crit, pattern, (err, result)=>{
            if(err){
                return res.status(500).send(err);
            }else{
                res.send(JSON.stringify(result));
            }
        });
    }else{
        res.send([]);
    }
};