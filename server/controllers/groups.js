var dao = require('../dao/mongodb/');
var async = require('async');

exports.create = function (req, res) {
    dao.create('groups', {
        group_name: req.body.group_name,
        group_title: req.body.group_title
        },(err, result)=>{
            if(err){
                return res.status(500).send(err);
            }
            res.send(JSON.stringify(result))
        });
};

exports.update = function (req, res) {
    async.waterfall([
        (next) => {
            dao.update('groups', {group_name: req.params.group_name}, {$set: req.body}, {}, next)
        },
        (next) => {
            dao.update('users', {groups: req.params.group_name}, {$set: {"groups.$": req.body.group_name}}, {multi: true}, next)
        },
        (next) => {
            dao.getOne('groups', {group_name: req.body.group_name}, (err, groupInfo)=>{
                next(null, groupInfo);
            });
        },
        (groupInfo, next) => {
            dao.get('users', {groups: groupInfo.group_name}, {groups: 0}, (err, usersList)=> {
                groupInfo.users = usersList || [];
                next(err, groupInfo);
            })
        }
    ], (err, group)=> {
        if (err) {
            return res.status(500).send(err);
        }
        res.send(JSON.stringify(group));
    });
};

exports.getOne = function (req, res) {
    var group = null;
    dao.getOne('groups', {group_name:req.params.group_name}, (err, groupInfo)=>{
        if(err){
            return res.status(500).send(err);
        }else if(!groupInfo){
            return res.status(404).send('Group not found')
        }
        group = groupInfo;
        dao.get('users', {groups: req.params.group_name}, {groups: 0}, (err, usersList)=>{
            if(err){
                return res.status(500).send(err);
            }
            group.users = usersList || [];
            res.send(JSON.stringify(group));
        })
    });
};

exports.delete = function (req, res) {
    dao.delete('groups', {group_name:req.params.group_name}, (err)=>{
        if(err){
            return res.status(500).send(err);
        }
        dao.update('users', {groups:req.params.group_name}, {$pull: {groups:req.params.group_name}}, {multi:true}, (err)=>{
            if(err){
                return res.status(500).send(err);
            }
        })
    })
};

exports.get = function (req, res) {
    var page = req.query.page || 0;
    var size = 5;
    var skip = (page > 0) ? ((page-1) * size) : 0;
    if(page != 0){
        dao.get('groups', null, {skip:skip, limit:size},
            (err, result)=>{
                if(err){
                    return res.status(500).send(err);
                }
                res.send(JSON.stringify(result))
            });
    }else{
        dao.get('groups', {}, {},
            (err, result)=>{
                if(err){
                    return res.status(404).send(err);
                }
                res.send(JSON.stringify(result.length))
            });
    }
};

exports.search = function (req, res) {
    var pattern = req.query.pattern;
    if(pattern != '^'){
        dao.search('groups', 'group_name', pattern, (err, result)=>{
            if(err){
                return res.status(500).send(err);
            }else{
                res.send(JSON.stringify(result));
            }
        })
    }else{
        res.send([]);
    }
};