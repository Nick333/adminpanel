var Mongo = require('mongodb').MongoClient;


var db =  null;

exports.connect = function (url, callback) {
    if (db) return callback(err, db);
    Mongo.connect(`mongodb://${url}`, (err, database)=>{
        if(err) {
            return callback(new Error('Connection failed'));
        }
        db = database;
        callback(err, db);
    })
};

exports.init = function () {
    db.collection('users').createIndex({
        username: 1,
        email: 1
    },{
        unique: true
    });

    db.collection('groups').createIndex({
        group_name: 1
    },{
        unique:true
    });
};

exports.getConnection = function () {
    return db;
};

exports.create = function (collection, query, callback) {
    db.collection(collection).insert(query, (err)=>{
        if(err){
            return callback(new Error('Insert error'));
        }
        db.collection(collection).findOne(query, callback);
    })
};

exports.get = function (collection, query = {}, options = {}, callback) {
    db.collection(collection).find(query, options).toArray((err, result)=>{
        if(err){
            return callback(new Error('Cannot get from ' + collection))
        }
        callback(err, result);
    })
};

exports.delete = function (collection, query, callback){
    db.collection(collection).deleteOne(query, (err)=>{
        if(err){
            callback(new Error('Delete error'));
        }
        callback();
    })
};

exports.getOne = function (collection, query, callback) {
    db.collection(collection).findOne(query, (err, result)=>{
        if(err){
            return callback(new Error('Cannot get from ' + collection));
        }
        callback(err, result);
    })
};

exports.update = function (collection, query, update, options, callback) {
    db.collection(collection).update(query, update, options, (err)=>{
        if(err){
            return callback(new Error('Update error'));
        }
        callback();
    })
};

exports.search = function (collection, crit, pattern, callback) {
    db.collection(collection).find({[crit]:{$regex:pattern, $options:'i'}}).toArray((err, result)=>{
        if(err){
            return callback(new Error('Not found in ' + collection));
        }
        callback(err, result);
    })
};

exports.getNotInGroup = function (query, callback) {
    db.collection('users').find(query).toArray((err, result)=>{
        if(err){
            return callback(new Error('Cannot get from users'));
        }
        callback(err, result);
    })
};