import React from 'react';
import Header from '../components/Header';
import '../../dist/css/common.css';

export default class App extends React.Component{
    render(){
        return <div className="global-container">
            <Header />
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-12">
                        {this.props.children}
                    </div>
                </div>
            </div>
        </div>
    }
}