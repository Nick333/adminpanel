import React from 'react';
import UsersTable from '../components/UsersTable';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as UserActions from '../actions/userActions';
import routingAction from '../actions/routingAction';
import {openModal} from '../actions/openModal';
import UserModalAdd from '../components/UserModalAdd';
import Overlay from '../components/Overlay';

class UsersList extends React.Component{
    componentWillMount(){
        this.props.dispatch(UserActions.getAllUsers());
    }

    componentWillUpdate(nextProps){
      if(nextProps.usersArray.length != this.props.usersArray.length){
            this.props.dispatch(UserActions.getUsersCount());
       }
    }


    render(){
        var actions = bindActionCreators(UserActions, this.props.dispatch);
        var routeAction = bindActionCreators(routingAction, this.props.dispatch);
        var showModal = bindActionCreators(openModal, this.props.dispatch);
        var pagesCount = Math.ceil(this.props.usersCount/5);
        var paginationItems = [];
        for(let i=1; i<=pagesCount; i++){
            paginationItems.push(<li key={i}><a onClick={()=>actions.getAllUsers(i)}>{i}</a></li>);
        }
        return <div>
            {this.props.status.loading === true ? <strong>LOADING...</strong> :
            <UsersTable users={this.props.usersArray}
                        getUsername={actions.getUsername}
                        route={routeAction}
                        search={actions.searchUsers}
                        searchedUsers={this.props.searchedUsers}/>
            }
            <button type="button" className="btn btn-primary" onClick={()=>showModal(true)}>ADD USER</button>
            <UserModalAdd style={{
                display: this.props.isOpenModal === true ? 'block' : 'none'
            }} showModal={showModal} saveFunc={actions.addUser} />
            {((this.props.searchedUsers.length == 0) && (this.props.usersCount > 5)) ?
            <nav aria-label="Page navigation">
                <ul className="pagination">
                    {
                        paginationItems.map(item=>{
                            return item;
                        })
                    }
                </ul>
            </nav> : null}
            <Overlay showModal={showModal}  style={{
                display: this.props.isOpenModal == true ? 'block' : 'none'
            }} />
            </div>
    }
}

export default connect(state=>({
    usersArray: state.users.users,
    status: state.users,
    usersCount: state.usersCount,
    isOpenModal: state.isOpenModal,
    searchedUsers: state.searchedUsers
}))(UsersList);
