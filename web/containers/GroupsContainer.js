import React from 'react';
import BreadCrumbs from '../components/BreadCrumbs';
import { connect } from 'react-redux';

class GroupsContainer extends React.Component{

    render(){
        var bcs = this.props.location.split('/');
        bcs.splice(0,1);
        return <div className="content-container">
            <BreadCrumbs breadcrumbs={bcs}/>
            {this.props.children}
        </div>
    }
}

export default connect(state=>({
    location: state.routing.locationBeforeTransitions.pathname
}))(GroupsContainer);