import React from 'react';
import GroupInfo from '../components/GroupInfo';
import '../../dist/css/current-group.css';
import {openModal} from '../actions/openModal';
import {bindActionCreators} from 'redux';
import * as GroupActions from '../actions/groupActions';
import { connect } from 'react-redux';
import routingAction from '../actions/routingAction';

class GroupInfoContainer extends React.Component{

    componentWillMount(){
        var gname = this.props.group_name || this.props.params.group_name;
        this.props.dispatch(GroupActions.getGroup(gname));
    }

    render(){
        var actions = bindActionCreators(GroupActions, this.props.dispatch);
        var showModal = bindActionCreators(openModal, this.props.dispatch);
        var index = this.props.groups.findIndex((group, ind, arr)=>{
            return group.group_name = this.props.group_name;
        });

        var gName = this.props.group.group_name;
        var routeAction = bindActionCreators(routingAction, this.props.dispatch);
        return <GroupInfo group={this.props.group} status={this.props.status} index={index} saveFunc={actions.editGroup} style={{
            display: this.props.isOpenModal == true ? 'block' : 'none'
        }} showModal={showModal} ind={index} group_name={gName} route={routeAction} deleteGroup={actions.deleteGroup}/>
    }
}

export default connect(state=>({
    group_name: state.groupname,
    group: state.singleGroup.group,
    status: state.singleGroup,
    groups: state.groups.groups,
    isOpenModal: state.isOpenModal
}))(GroupInfoContainer);