import React from 'react';
import BreadCrumbs from '../components/BreadCrumbs';
import '../../dist/css/users.css';
import { connect } from 'react-redux';

class UsersContainer extends React.Component{

    render(){
        var bcs = this.props.location.split('/');
        bcs.splice(0,1);
        return <div className="content-container">
                <BreadCrumbs breadcrumbs={bcs}/>
                {this.props.children}
            </div>
    }
}

export default connect(state=>({
    location: state.routing.locationBeforeTransitions.pathname
}))(UsersContainer);