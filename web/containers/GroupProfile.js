import React from 'react';
import { Link } from 'react-router';

export default class GroupProfile extends React.Component{

    render(){
        return <div className="panel panel-default">
                <ul className="nav nav-tabs">
                    <li role="presentation"><Link className="gprofile-nav" activeClassName="active" onlyActiveOnIndex={true} to={`/groups/${this.props.params.group_name}`}>Group info</Link></li>
                    <li role="presentation"><Link className="gprofile-nav" activeClassName="active" to={`/groups/${this.props.params.group_name}/users`}>Group users</Link></li>
                </ul>
                {this.props.children}
                </div>
    }

}
