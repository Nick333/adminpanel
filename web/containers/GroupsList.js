import React from 'react';
import GroupsTable from '../components/GroupsTable';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as GroupActions from '../actions/groupActions';
import '../../dist/css/groups.css';
import routingAction from '../actions/routingAction';
import GroupModalAdd from '../components/GroupModalAdd';
import { openModal } from '../actions/openModal';
import Overlay from '../components/Overlay';

class GroupsList extends React.Component{
    componentWillMount(){
        this.props.dispatch(GroupActions.getAllGroups());
    }

    componentWillUpdate(nextProps){
        if(nextProps.groupsArray.length != this.props.groupsArray.length){
            this.props.dispatch(GroupActions.getGroupsCount());
        }
    }

    render(){
        var actions = bindActionCreators(GroupActions, this.props.dispatch);
        var routeAction = bindActionCreators(routingAction, this.props.dispatch);
        var showModal = bindActionCreators(openModal, this.props.dispatch);
        var pagesCount = Math.ceil(this.props.groupsCount/5);
        var paginationItems = [];
        for(let i=1; i<=pagesCount; i++){
            paginationItems.push(<li key={i}><a onClick={()=>actions.getAllGroups(i)}>{i}</a></li>);
        }
        return  <div>
            {this.props.status.loading === true ? <strong>LOADING...</strong> :
                    <GroupsTable
                        groups={this.props.groupsArray}
                        getGroupname={actions.getGroupname}
                        route={routeAction}
                        search={actions.searchGroups}
                        searchedGroups={this.props.searchedGroups}/> }
                    <button type="button" className="btn btn-primary" onClick={()=>showModal(true)}>ADD GROUP</button>
                    <GroupModalAdd style={{display: this.props.isOpenModal == true ? 'block' : 'none'}} saveFunc={actions.addGroup} showModal={showModal}/>
                    {((this.props.searchedGroups.length == 0) && (this.props.groupsCount > 5)) ?
                        <nav aria-label="Page navigation">
                            <ul className="pagination">
                                {
                                    paginationItems.map(item=>{
                                        return item;
                                    })
                                }
                            </ul>
                        </nav> : null}
                <Overlay showModal={showModal}  style={{
                    display: this.props.isOpenModal == true ? 'block' : 'none'
                }} />
                </div>
    }
}

export default connect(state=>({
    status: state.groups,
    searchedGroups: state.searchedGroups,
    groupsCount: state.groupsCount,
    groupsArray: state.groups.groups,
    isOpenModal: state.isOpenModal
}))(GroupsList)