import React from 'react';
import UserInfo from '../components/UserInfo';
import GroupsTable from '../components/GroupsTable';
import  '../../dist/css/current-user.css';
import { connect } from 'react-redux';
import * as UserActions from '../actions/userActions';
import {bindActionCreators} from 'redux';
import { getGroupname } from '../actions/groupActions';
import routingAction from '../actions/routingAction';
import UserModalEdit from '../components/UserModalEdit';
import {openModal} from '../actions/openModal';
import Overlay from '../components/Overlay';

class UserProfile extends React.Component{
    componentWillMount(){
        var uname = this.props.username || this.props.user.username || this.props.params.username;
        this.props.dispatch(UserActions.getUser(uname));
    }

    render(){
        var actions = bindActionCreators(UserActions, this.props.dispatch);
        var routeAction = bindActionCreators(routingAction, this.props.dispatch);
        var getGrName = bindActionCreators(getGroupname, this.props.dispatch);
        var showModal = bindActionCreators(openModal, this.props.dispatch);
        var index = this.props.users.findIndex((user, ind, arr)=>{
            return user.username = this.props.username;
        });
        var uName = this.props.user.username;
        return <div className="panel panel-default">
            <div className="panel-heading">
                <h3 className="panel-title">{this.props.user.username}</h3>
            </div>
            { this.props.status.loading === true ? <strong>LOADING...</strong>
            :
            <div className="panel-body cur-user">
                <UserInfo user={this.props.user} showModal={showModal} deleteUser={actions.deleteUser} route={routeAction}/>
                <div className="user-groups">
                    <GroupsTable
                        groups={this.props.user.groups}
                        getGroupname={getGrName}
                        route={routeAction} />
                </div>
            </div>}
            <UserModalEdit style={{
                display: this.props.isOpenModal === true ? 'block' : 'none'
            }} showModal={showModal} index={index} saveFunc={actions.editUser} userName={uName} user={this.props.user} route={routeAction}/>
            <Overlay showModal={showModal}  style={{
                display: this.props.isOpenModal == true ? 'block' : 'none'
            }} />
        </div>
    }
}

export default connect(state=>({
    username: state.username,
    user: state.singleUser.user,
    status: state.singleUser,
    users: state.users.users,
    isOpenModal: state.isOpenModal
}))(UserProfile);

