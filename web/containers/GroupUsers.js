import React from 'react';
import UsersTable from '../components/UsersTable';

export default class GroupUsers extends React.Component{

    render(){
        return <div>
            <UsersTable/>
            <button type="submit" className="btn btn-primary">ADD USER TO GROUP</button>
        </div>
    }

}