import React from 'react';
import UsersTable from '../components/UsersTable';
import { connect } from 'react-redux';
import routingAction from '../actions/routingAction';
import {bindActionCreators} from 'redux';
import { getUsername, getNewUsers } from '../actions/userActions';
import { getGroup, editGroup, addUserToGroup } from '../actions/groupActions';
import UsersToAddModal from '../components/UsersToAddModal';
import {openModal} from '../actions/openModal';
import Overlay from '../components/Overlay';

class GroupUsersContainer extends React.Component{

    componentWillMount(){
        this.props.dispatch(getNewUsers(this.props.params.group_name));
        this.props.dispatch(getGroup(this.props.params.group_name));
    }


    render(){
        var showModal = bindActionCreators(openModal, this.props.dispatch);
        var addUserToGr =  bindActionCreators(addUserToGroup, this.props.dispatch);
       // var editGr = bindActionCreators(editGroup, this.props.dispatch);
        var index = this.props.groupsArr.findIndex((group, index, arr)=>{
            return group.group_name == this.props.group_name;
        });
        var groupname = this.props.group_name || this.props.params.group_name;
        var usersToAdd = this.props.notInGroupUsers;
        var gr = {
            index,
            groupname
        };
        var route = bindActionCreators(routingAction, this.props.dispatch);
        var getUserName =bindActionCreators(getUsername, this.props.dispatch);
        return <div>
            {this.props.status.loading === true ? <strong>LOADING...</strong> :
            <UsersTable
                users={this.props.group.users}
                route={route}
                getUsername={getUserName}/> }
            <button type="submit" className="btn btn-primary" onClick={()=>{showModal(true); this.props.dispatch(getNewUsers(this.props.params.group_name))}}>ADD USER TO GROUP</button>
            <UsersToAddModal
                style={{display: this.props.isOpenModal == true ? 'block' : 'none'}}
                showModal={showModal}
                users={usersToAdd}
                addUserToGr={addUserToGr}
                gr={gr}/>
            <Overlay showModal={showModal}  style={{
                display: this.props.isOpenModal == true ? 'block' : 'none'
            }} />
            </div>
    }

}

export default connect(state=>({
    status: state.singleGroup,
    groupsArr: state.groups.groups,
    group: state.singleGroup.group,
    notInGroupUsers: state.notInGroupUsers,
    isOpenModal: state.isOpenModal,
    group_name: state.groupname
}))(GroupUsersContainer)