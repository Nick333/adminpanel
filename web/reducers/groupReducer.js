import { GET_GROUP, EDIT_GROUP, ADD_USER_TO_GROUP } from '../actions/groupActions';

var initialState = {
    loading: false,
    group: {
        users: []
    },
    error: false
};

export default function groupReducer(state = initialState, action) {
    switch (action.type){
        case `${GET_GROUP}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${GET_GROUP}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case GET_GROUP:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                group: action.payload
            });
        case EDIT_GROUP:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                group: Object.assign({}, action.payload.group, {
                    group_name: action.payload.group.group_name,
                    group_title: action.payload.group.group_title,
                    users: action.payload.group.users
                })

            });
        case ADD_USER_TO_GROUP:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                group: Object.assign({}, state.group, {
                    users: [
                        ...state.group.users,
                        action.payload
                    ]
                })
            });
        default:
            return state;
    }
}