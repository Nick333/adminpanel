import { GET_NEW_USERS } from '../actions/userActions';

export default function notInGroupUsersReducer(state = [], action) {
    switch (action.type){
        case GET_NEW_USERS:
            return action.payload || state;
        default:
            return state;
    }
}