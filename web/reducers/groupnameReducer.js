import { GET_GROUP_NAME } from '../actions/groupActions';

export default function groupnameReducer(state = '', action) {
    switch (action.type){
        case GET_GROUP_NAME:
            return action.payload;
        default:
            return state;
    }
}