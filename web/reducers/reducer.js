import { combineReducers } from 'redux';
import usersReducer from './usersReducer';
import userReducer from './userReducer';
import groupsReducer from './groupsReducer';
import groupReducer from './groupReducer';
import usernameReducer from './usernameReducer';
import groupnameReducer from './groupnameReducer';
import isOpenModal from './isOpenModal';
import usersCountReducer from './usersCountReducer';
import { routerReducer } from 'react-router-redux';
import notInGroupUsersReducer from './notInGroupUsersReducer';
import searchedUsersReducer from './searchedUsersReducer';
import groupsCountReducer from './groupsCountReducer';
import searchedGroupsReducer from './searchedGroupsReducer';

export default combineReducers({
    users: usersReducer,
    singleUser: userReducer,
    singleGroup: groupReducer,
    groups: groupsReducer,
    username: usernameReducer,
    groupname: groupnameReducer,
    usersCount: usersCountReducer,
    routing: routerReducer,
    searchedGroups: searchedGroupsReducer,
    groupsCount: groupsCountReducer,
    searchedUsers: searchedUsersReducer,
    notInGroupUsers: notInGroupUsersReducer,
    isOpenModal
});