import { SEARCH_USERS } from '../actions/userActions';


export default function searchedUsersReducer(state = [], action) {
    switch (action.type){
        case SEARCH_USERS:
            return action.payload || state;
        default:
            return state;
    }
}