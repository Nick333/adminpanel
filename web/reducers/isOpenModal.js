import { OPEN_MODAL } from '../actions/openModal';

export default function isOpenModal(state = false, action) {
    switch (action.type){
        case OPEN_MODAL:
            return action.payload;
        default:
            return state;
    }
}