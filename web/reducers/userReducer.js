import { GET_USER, EDIT_USER } from '../actions/userActions';

var initialState = {
    loading: false,
    user: {
        groups: []
    },
    error: false
};

export default function userReducer(state = initialState, action) {
    switch (action.type){
        case `${GET_USER}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${GET_USER}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case GET_USER:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                user: action.payload
            });
        case EDIT_USER:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                user: action.payload.user
            });
        default:
            return state;
    }
}