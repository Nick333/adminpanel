import { GET_USERNAME } from '../actions/userActions';

export default function usernameReducer(state = '', action) {
    switch (action.type){
        case GET_USERNAME:
            return action.payload;
        default:
            return state;
    }
}