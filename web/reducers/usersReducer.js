import { GET_ALL_USERS, ADD_USER, EDIT_USER, DELETE_USERS } from '../actions/userActions';

const initialState = {
    loading: false,
    users: [],
    error: false
};

export default function usersReducer(state = initialState, action) {
    switch (action.type){
        case `${GET_ALL_USERS}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${GET_ALL_USERS}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case GET_ALL_USERS:
            return Object.assign({}, state, {
                loading: false,
                error:false,
                users: action.payload
            });
        case `${ADD_USER}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${ADD_USER}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case ADD_USER:
                return Object.assign({}, state, {
                    loading: false,
                    error: false,
                    users: [
                        ...state.users,
                        action.payload
                    ]
                });
        case `${EDIT_USER}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${EDIT_USER}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        /*case EDIT_USER:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                users: [
                    ...state.users.slice(0, action.payload.index),
                    Object.assign({}, state.users[action.payload.index], action.payload.user),
                    ...state.users.slice(action.payload.index+1)
                ]
            });*/
        default:
            return state;
    }
}