import { GET_GROUPS_COUNT } from '../actions/groupActions'


export default function groupsCountReducer(state = 0, action) {
    switch(action.type){
        case GET_GROUPS_COUNT:
            return action.payload || state;
        default:
            return state;
    }
}