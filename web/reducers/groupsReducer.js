import { GET_ALL_GROUPS, ADD_GROUP, EDIT_GROUP, DELETE_GROUP} from '../actions/groupActions';

const initialState = {
    loading: false,
    groups: [],
    error: false
};

export default function groupsReducer(state = initialState, action) {
    switch (action.type){
        case `${GET_ALL_GROUPS}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${GET_ALL_GROUPS}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case GET_ALL_GROUPS:
            return Object.assign({}, state, {
                loading: false,
                error: false,
                groups: action.payload
            });
        case `${ADD_GROUP}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${ADD_GROUP}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case ADD_GROUP:
                return Object.assign({}, state, {
                    loading: false,
                    error: false,
                    groups: [
                        ...state.groups,
                        action.payload
                    ]
                });
        case `${EDIT_GROUP}_LOADING`:
            return Object.assign({}, state, {
                loading: true
            });
        case `${EDIT_GROUP}_ERROR`:
            return Object.assign({}, state, {
                loading: false,
                error: true
            });
        case EDIT_GROUP:
            var usersNamesArr = action.payload.group.users.map(user=>{
                return user.username;
            });
            return Object.assign({}, state, {
                loading: false,
                error: false,
                groups: [
                    ...state.groups.slice(0,action.payload.index),
                    Object.assign({}, state.groups[action.payload.index], action.payload.group, {users:usersNamesArr}),
                    ...state.groups.slice(action.payload.index+1)
                ]
            });
        default:
            return state;
    }
}