import { SEARCH_GROUPS } from '../actions/groupActions';


export default function searchedGroupsReducer(state = [], action) {
    switch (action.type){
        case SEARCH_GROUPS:
            return action.payload || [];
        default:
            return state;
    }
}