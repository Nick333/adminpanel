import React from 'react';
import ReactDOM from 'react-dom';
require("bootstrap-webpack!../bootstrap.config.js");
import { routes } from './routes'
import { Router, browserHistory, Route, hashHistory } from 'react-router';
import { applyMiddleware, createStore, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { redirect } from './middlewares/redirect';
import reducer from './reducers/reducer';
import { syncHistoryWithStore } from 'react-router-redux';

/*--------------------------------------------------------------------*/

const store = createStore(reducer, {}, compose(
    applyMiddleware(
        thunk,
        redirect
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f
));

const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(<Provider store={store}>
                    <Router history={ history } routes={routes} />
                </Provider>,
    document.getElementById('root'));
