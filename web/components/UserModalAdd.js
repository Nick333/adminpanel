import React from 'react';

export default class UserModalAdd extends React.Component{
    constructor(props){
        super(props);
    }

    saveClick(status){
        var newUser = {
            username: this.refs.username.value,
            email: this.refs.email.value,
            first_name: this.refs.fname.value,
            last_name: this.refs.lname.value
        };
        var emptyPropsCounter=0;
        for(let key in newUser){
            if(!newUser[key]){
                emptyPropsCounter++;
            }
        }
        if(emptyPropsCounter == 0){
            this.props.saveFunc(newUser);
            this.props.showModal(status);
            document.forms['user-add-form'].reset();
        }
    }

    cancelClick(status){
        this.props.showModal(status);
        document.forms['user-add-form'].reset();
    }

    render(){
        return <div className="modal-form user" style={this.props.style}>
            <form className="form-horizontal" name="user-add-form">
                <div className="form-group">
                    <label forHtml="username-inp" className="col-sm-4 control-label">Username</label>
                    <div className="col-sm-8">
                        <input id="username-inp" ref="username" type="text" className="form-control" placeholder="Username" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="email-inp" className="col-sm-4 control-label">E-mail</label>
                    <div className="col-sm-8">
                        <input id="email-inp" type="email" ref="email" className="form-control" placeholder="E-mail" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="firstname-inp" className="col-sm-4 control-label">First name</label>
                    <div className="col-sm-8">
                        <input id="firstname-inp" type="email" ref="fname" className="form-control" placeholder="First name" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="lastname-inp" className="col-sm-4 control-label">Last name</label>
                    <div className="col-sm-8">
                        <input id="lastname-inp" type="email" ref="lname" className="form-control" placeholder="Last name" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <button type="button" className="btn btn-success" onClick={()=>this.saveClick(status)}>Save</button>
                        <button type="button" className="btn btn-danger" onClick={()=>this.cancelClick(status)}>Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    }
}