import React from  'react';

export default class GroupsTable extends React.Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.setState({
            groups: this.props.groups
        })
    }

    componentWillReceiveProps(nextProps){
        if(!this.refs.groupSearchField || (this.refs.groupSearchField.value == '')){
            this.setState({
                groups: nextProps.groups
            })
        }else{
            nextProps.searchedGroups ? this.setState({
                groups: nextProps.searchedGroups
            }) : this.setState({
                groups: nextProps.groups
            })
        }
    }

    searchFunc(){
        var groupSearchField = this.refs.groupSearchField;
        var pattern = groupSearchField.value;
        var regExp;
        if(this.props.search){
            regExp = `^${pattern}`;
            this.props.search(regExp);
            if(groupSearchField.value != ''){
                this.setState({
                    groups: this.props.searchedGroups
                })}else{
                this.setState({
                    groups: this.props.groups
                })
            }
        }else{
            regExp = new RegExp(`^${pattern}`, 'i');
            var filteredGroups = this.props.groups.filter(group=>{
                if(group.group_name.match(regExp) != null){
                    return group;
                }
            });
            this.setState({
                groups: filteredGroups
            })
        }
    }

    rowClick(groupname){
        this.props.getGroupname(groupname);
        this.props.route('push', `/groups/${groupname}`);
    }

    render(){
        return <div className="panel panel-default">
            <div className="panel-heading">
                    <p className="panel-title">Groups</p>
                    <form className="navbar-form navbar-left search-form" role="search">
                        <div className="form-group">
                            <input type="text" ref="groupSearchField" className="form-control" placeholder="Enter group name" onChange={()=>this.searchFunc()} />
                        </div>
                    </form>
                </div>
                <div className="panel-body">
                { (this.state.groups.length) ?
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th>Group name</th>
                            <th>Title</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.state.groups.map((group, index)=>{
                                if(((this.props.searchedGroups) && (this.props.searchedGroups.length > 0)) || !this.props.searchedGroups) {
                                    return <tr key={index} onClick={()=> {
                                        this.rowClick(group.group_name)
                                    }}>
                                        <td>{group.group_name}</td>
                                        <td>{group.group_title}</td>
                                    </tr>
                                }else if(index <= 4){
                                    return <tr key={index} onClick={()=> {
                                        this.rowClick(group.group_name)
                                    }}>
                                        <td>{group.group_name}</td>
                                        <td>{group.group_title}</td>
                                    </tr>
                                }
                            })
                        }
                        </tbody>
                    </table>
                    : <center><strong>There are no groups :(</strong></center>
                }</div>
        </div>
    }
}

GroupsTable.propTypes = {
    getGroupname: React.PropTypes.func,
    route: React.PropTypes.func,
    groups: React.PropTypes.array,
    search: React.PropTypes.func,
    searchedGroups: React.PropTypes.array
};