import React from 'react';

export default class UserModalEdit extends React.Component{
    constructor(props){
        super(props);
    }

    saveClick(status){
        this.props.showModal(status);
        var editedUser = {
            username: this.refs.username.value || this.props.user.username,
            email: this.refs.email.value || this.props.user.email,
            first_name: this.refs.fname.value || this.props.user.first_name,
            last_name: this.refs.lname.value || this.props.user.last_name
        };
        var index = this.props.index;

        this.props.saveFunc(this.props.userName, editedUser, index);
        document.forms['user-edit-form'].reset();
    }

    componentWillUpdate(nextProps){
        if(nextProps.userName != this.props.userName){
            this.props.route('push', `/users/${nextProps.userName}`)
        }
    }

    cancelClick(status){
        this.props.showModal(status);
        document.forms['user-edit-form'].reset();
    }

    render(){
        return <div className="modal-form user" style={this.props.style}>
            <form className="form-horizontal" name="user-edit-form">
                <div className="form-group">
                    <label forHtml="username-inp" className="col-sm-4 control-label">Username</label>
                    <div className="col-sm-8">
                        <input id="username-inp" ref="username" type="text" className="form-control" placeholder="Username" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="email-inp" className="col-sm-4 control-label">E-mail</label>
                    <div className="col-sm-8">
                        <input id="email-inp" type="email" ref="email" className="form-control" placeholder="E-mail" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="firstname-inp" className="col-sm-4 control-label">First name</label>
                    <div className="col-sm-8">
                        <input id="firstname-inp" type="email" ref="fname" className="form-control" placeholder="First name" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="lastname-inp" className="col-sm-4 control-label">Last name</label>
                    <div className="col-sm-8">
                        <input id="lastname-inp" type="email" ref="lname" className="form-control" placeholder="Last name" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <button type="button" className="btn btn-success" onClick={()=>this.saveClick(status)}>Save</button>
                        <button type="button" className="btn btn-danger" onClick={()=>this.cancelClick(status)}>Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    }
}