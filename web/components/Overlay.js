import React from 'react';

export default class Overlay extends React.Component{

    render(){
        return <div id="overlay" style={this.props.style} onClick={()=>this.props.showModal(false)}></div>
    }
}