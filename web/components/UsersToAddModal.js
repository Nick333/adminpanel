import React from 'react';
import UsersTable from './UsersTable';
import '../../dist/css/current-group-users.css';

export default class UsersToAddModal extends React.Component{
    render(){
        return <div className="modal-form group-users" style={this.props.style}>
            <h2>Select user to add</h2>
            <UsersTable
                showModal={this.props.showModal}
                users={this.props.users}
                addUserToGr={this.props.addUserToGr}
                gr={this.props.gr}/>
        </div>
    }

}