import React from 'react';


export default class GroupModalEdit extends React.Component{
    constructor(props){
        super(props);
    }

    saveClick(status){
        this.props.showModal(status);
        var editedGroup = {
            group_name: this.refs.groupName.value || this.props.group.group_name,
            group_title: this.refs.groupTitle.value || this.props.group.group_title
        };
        var index = this.props.ind;
        this.props.saveFunc(this.props.group_name, editedGroup, index);
        /*setTimeout(()=>{
            this.props.route('push', `/groups/${this.props.group_name}`)
        }, 500);*/
    }

    componentWillUpdate(nextProps){
        if(nextProps.group_name != this.props.group_name){
            this.props.route('push', `/groups/${nextProps.group_name}`)
        }
    }

    cancelClick(status){
        this.props.showModal(status);
    }

    render(){
        return <div className="modal-form group" style={this.props.style}>
            <form className="form-horizontal">
                <div className="form-group">
                    <label forHtml="groupname-inp" className="col-sm-4 control-label">Group name</label>
                    <div className="col-sm-8">
                        <input id="groupname-inp" ref="groupName" type="text" className="form-control" placeholder="Group name" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="title-inp" className="col-sm-4 control-label">Title</label>
                    <div className="col-sm-8">
                        <input id="title-inp" type="text" ref="groupTitle" className="form-control" placeholder="Title" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <button type="button" className="btn btn-success" onClick={()=>this.saveClick(false)}>Save</button>
                        <button type="button" className="btn btn-danger" onClick={()=>this.cancelClick(false)}>Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    }
}