import React from 'react';
import { Link } from 'react-router';

export default class BreadCrumbs extends React.Component{

    render(){
        var path='';
        return  <ol className="breadcrumb">
                {this.props.breadcrumbs.map((bc, ind)=>{
                    bc = bc.charAt(0).toUpperCase() + bc.substr(1);
                    path = path.concat('/', bc);
                    return <li key={ind}><Link key={ind} to={path.toLowerCase()} activeClassName="active" onlyActiveOnIndex={true} >{bc}</Link></li>
                })}
                </ol>
    }
}