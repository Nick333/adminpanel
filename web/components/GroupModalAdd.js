import React from 'react';


export default class GroupModalAdd extends React.Component{

    saveClick(status){
        var newGroup = {
            group_name: this.refs.groupName.value,
            group_title: this.refs.groupTitle.value
        };
        var emptyPropsCounter=0;
        for(let key in newGroup){
            if(!newGroup[key]){
                emptyPropsCounter++;
            }
        }
        if(emptyPropsCounter == 0){
            this.props.saveFunc(newGroup);
            this.props.showModal(status);
            document.forms['group-add-form'].reset();
        }
    }

    cancelClick(status){
        this.props.showModal(status);
        document.forms['group-add-form'].reset();
    }

    render(){
        return <div className="modal-form group" style={this.props.style}>
            <form className="form-horizontal" name="group-add-form">
                <div className="form-group">
                    <label forHtml="groupname-inp" className="col-sm-4 control-label">Group name</label>
                    <div className="col-sm-8">
                        <input id="groupname-inp" ref="groupName" type="text" className="form-control" placeholder="Group name" />
                    </div>
                </div>
                <div className="form-group">
                    <label forHtml="title-inp" className="col-sm-4 control-label">Title</label>
                    <div className="col-sm-8">
                        <input id="title-inp" ref="groupTitle" type="text" className="form-control" placeholder="Title" />
                    </div>
                </div>
                <div className="form-group">
                    <div className="col-sm-offset-2 col-sm-10">
                        <button type="button" className="btn btn-success" onClick={()=>this.saveClick(false)}>Save</button>
                        <button type="button" className="btn btn-danger" onClick={()=>this.cancelClick(false)}>Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    }
}