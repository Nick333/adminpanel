import React from 'react';
import GroupModalEdit from '../components/GroupModalEdit';
import Overlay from '../components/Overlay';

export default class GroupInfo extends React.Component{
    constructor(props){
        super(props);
    }

    deleteClick(gname){
        this.props.deleteGroup(gname);
        this.props.route('push', '/groups');
    }

    render(){
        return <div>
            <div className="panel-heading">
                <h3 className="panel-title">{this.props.group.group_name}</h3>
            </div>
            <div className="panel-body group">
                <div className="group-info">
                    <div className="group-info-row">
                        <p className="group-info-title">Group name: </p><p className="group-info-text">{this.props.group.group_name}</p>
                    </div>
                    <div className="group-info-row">
                        <p className="group-info-title">Title: </p><p className="group-info-text">{this.props.group.group_title}</p>
                    </div>
                    <button type="button" className="btn btn-primary" onClick={()=>this.props.showModal(true)}>Edit</button>
                    <button type="button" className="btn btn-primary" onClick={()=>this.deleteClick(this.props.group.group_name)}>Delete</button>
                </div>
            </div>
            <GroupModalEdit style={this.props.style} saveFunc={this.props.saveFunc} showModal={this.props.showModal} ind={this.props.ind} group_name={this.props.group_name} group={this.props.group} route={this.props.route} />
            <Overlay showModal={this.props.showModal} style={this.props.style}/>
        </div>
    }
}