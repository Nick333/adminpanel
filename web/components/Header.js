import React from 'react';
import NavLink from './NavLink';

export default class Header extends  React.Component{

    render(){
        return <header id="header">
                <div className="header-inner">
                    <nav className="nav-container">
                        <ul className="nav nav-pills">
                            <li role="presentation"><NavLink className="header-nav" to="/users">Users</NavLink></li>
                            <li role="presentation"><NavLink className="header-nav" to="/groups">Groups</NavLink></li>
                        </ul>
                    </nav>
                </div>
            </header>
    }
}