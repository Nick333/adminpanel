import React from 'react';


export default class UsersTable extends React.Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.setState({
            users:this.props.users
        })
    }

   componentWillReceiveProps(nextProps){
        if((this.refs.userSearchField.value == '')){
            this.setState({
                users: nextProps.users
            })
        }else{
            nextProps.searchedUsers ? this.setState({
                users: nextProps.searchedUsers
            }) : this.setState({
                users: nextProps.users
            })
        }
    }

    searchFunc(){
        var userSearchField = this.refs.userSearchField;
        var pattern = userSearchField.value;
        var criterion = this.refs.criterion;
        var crit = criterion.value;
        var regExp;
        if(this.props.search){
            regExp = `^${pattern}`;
            this.props.search(regExp, crit);
            if(userSearchField.value != ''){
                this.setState({
                    users: this.props.searchedUsers
                })}else{
                this.setState({
                    users: this.props.users
                })
            }
        }else{
            regExp = new RegExp(`^${pattern}`, 'i');
            var filteredUsers = this.props.users.filter(user=>{
                if(user[crit].match(regExp) != null){
                    return user
                }
            });
            this.setState({
                users: filteredUsers
            })
        }

    }

    rowClick(username){
            if(this.props.showModal){
                this.props.showModal(false);
            }
            if(this.props.addUserToGr){
                this.props.addUserToGr(username, this.props.gr.groupname);
            }
            for(let key in this.props){
                if((typeof this.props[key] == 'function') && (key != 'route') && (key != 'showModal') && (key != 'addUserToGr') && (key != 'search')){
                    this.props[key](username);
                }else if(key == 'route'){
                    this.props[key]('push', `/users/${username}`);
                }
            }
    }

    render(){
        return <div className="panel panel-default">
            <div className="panel-heading">
                <p className="panel-title">Users</p>
                <form className="navbar-form navbar-left search-form" role="search">
                    <select className="form-control" ref="criterion">
                        <option value="username">username</option>
                        <option value="email">email</option>
                        <option value="first_name">first name</option>
                        <option value="last_name">last name</option>
                    </select>
                    <div className="form-group">
                        <input type="text" ref="userSearchField" className="form-control" placeholder="Search" onChange={()=>this.searchFunc()}/>
                    </div>
                </form>
            </div>
            <div className="panel-body">
            {
                (this.state.users.length) ?
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th>Username</th>
                            <th>E-mail</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.users.map((user, ind)=>{
                            if(((this.props.searchedUsers) && (this.props.searchedUsers.length > 0)) || !this.props.searchedUsers){
                                return <tr key={ind} onClick={()=>this.rowClick(user.username)}>
                                    <td>{user.username}</td>
                                    <td>{user.email}</td>
                                    <td>{user.first_name}</td>
                                    <td>{user.last_name}</td>
                                </tr>
                            }else if((ind <= 4)){
                                return <tr key={ind} onClick={()=>this.rowClick(user.username)}>
                                    <td>{user.username}</td>
                                    <td>{user.email}</td>
                                    <td>{user.first_name}</td>
                                    <td>{user.last_name}</td>
                                </tr>
                            }
                        })
                        }
                        </tbody>
                    </table>
                    : <center><strong>There are no users :(</strong></center>
            }
            </div>

        </div>
    }
}

UsersTable.propTypes = {
    getUsername: React.PropTypes.func,
    route: React.PropTypes.func || undefined,
    users: React.PropTypes.array,
    search: React.PropTypes.func,
    searchedUsers: React.PropTypes.array
};


