import React from 'react';


export default class UserInfo extends React.Component{

    deleteClick(uname){
        this.props.deleteUser(uname);
        this.props.route('push', '/users');
    }

    render(){
        return <div className="user-info">
                    <div className="profile-info-row">
                        <p className="profile-info-title">Username: </p><p className="profile-info-text">{this.props.user.username}</p>
                    </div>
                    <div className="profile-info-row">
                        <p className="profile-info-title">E-mail: </p><p className="profile-info-text">{this.props.user.email}</p>
                    </div>
                    <div className="profile-info-row">
                        <p className="profile-info-title">First name: </p><p className="profile-info-text">{this.props.user.first_name}</p>
                    </div>
                    <div className="profile-info-row">
                        <p className="profile-info-title">Last name: </p><p className="profile-info-text">{this.props.user.last_name}</p>
                    </div>
                    <button type="button" className="btn btn-primary" onClick={()=>this.props.showModal(true)}>Edit</button>
                    <button type="button" className="btn btn-primary" onClick={()=>this.deleteClick(this.props.user.username)}>Delete</button>
                </div>
    }

}