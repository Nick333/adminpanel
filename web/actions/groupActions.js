export const GET_ALL_GROUPS = 'GET_ALL_GROUPS';
export const ADD_GROUP= 'ADD_GROUP';
export const GET_GROUP = 'GET_GROUP';
export const EDIT_GROUP = 'EDIT_GROUP';
export const GET_GROUP_NAME = 'GET_GROUP_NAME';
export const GET_GROUPS_COUNT = 'GET_GROUPS_COUNT';
export const SEARCH_GROUPS = 'SEARCH_GROUPS';
export const DELETE_GROUP = 'DELETE_GROUP';
export const ADD_USER_TO_GROUP = 'ADD_USER_TO_GROUP';

export function getAllGroups(page = 1) {
    return dispatch=>{
        dispatch({
            type: `${GET_ALL_GROUPS}_LOADING`
        });
        fetch(`${window.location.origin}/api/groups?page=${page}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(groups=>dispatch({
                    type: GET_ALL_GROUPS,
                    payload: groups
                })
            )
            .catch(err=>{
                dispatch({
                    type: `${GET_ALL_GROUPS}_ERROR`
                })
            })
    }
}

export function getGroupsCount() {
    return dispatch=>{
        fetch(`${window.location.origin}/api/groups`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(groupsCount=>dispatch({
                    type: GET_GROUPS_COUNT,
                    payload: +groupsCount
                })
            )
    }
}

export function searchGroups(pattern) {
    return dispatch=>{
        fetch(`${window.location.origin}/api/groups/search/?pattern=${pattern}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(groups=>dispatch({
                type: SEARCH_GROUPS,
                payload: groups
            }))
    }
}

export function deleteGroup(group_name) {
    return dispatch=>{
        fetch(`${window.location.origin}/api/groups/${group_name}`, {
            method: 'delete'
        })
            .then(()=>dispatch({
                type: DELETE_GROUP
        }))
    }
}

export function addGroup(group) {
    return dispatch=>{
        dispatch({
            type: `${ADD_GROUP}_LOADING`
        });
        fetch(`${window.location.origin}/api/groups`,{
            method: 'post',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                group_name: group.group_name,
                group_title: group.group_title
            })
        })
            .then(response=>response.json())
            .then(group=>dispatch({
                type: ADD_GROUP,
                payload: group
            }))
            .catch(err=>dispatch({
                type: `${ADD_GROUP}_ERROR`
            }))
    }
}

export function getGroup(group_name) {
    return dispatch=>{
        dispatch({
            type: `${GET_GROUP}_LOADING`
        });
        fetch(`${window.location.origin}/api/groups/${group_name}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(group=>dispatch({
                type: GET_GROUP,
                payload: group
            }))
            .catch(err=>dispatch({
                type: `${GET_GROUP}_ERROR`
            }))
    }
}

export function addUserToGroup(username, group_name) {
    return dispatch=>{
        fetch(`${window.location.origin}/api/users/${username}`, {
            method: 'PATCH',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({group_name})
        })
            .then(response=>response.json())
            .then((user)=>dispatch({
                type: ADD_USER_TO_GROUP,
                payload: user
            }));
    }
}

export function editGroup(group_name, group, index) {
    return dispatch=>{
        dispatch({
            type: `${EDIT_GROUP}_LOADING`
        });
        fetch(`${window.location.origin}/api/groups/${group_name}`,{
            method: 'put',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(group)
        })
            .then(response=>response.json())
            .then(groupInfo=>dispatch({
                type: EDIT_GROUP,
                payload: {
                    index,
                    group: groupInfo
                }
            }))
            .catch(err=>dispatch({
                type: `${EDIT_GROUP}_ERROR`
            }))
    }
}

export function getGroupname(groupname) {
    return {
        type: GET_GROUP_NAME,
        payload: groupname
    }
}