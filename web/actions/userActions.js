export const GET_ALL_USERS = 'GET_ALL_USERS';
export const ADD_USER = 'ADD_USER';
export const GET_USER = 'GET_USER';
export const EDIT_USER = 'EDIT_USER';
export const GET_USERNAME = 'GET_USERNAME';
export const GET_USERS_COUNT = 'GET_TOTAL_USERS_COUNT';
export const SEARCH_USERS = 'SEARCH_USERS';
export const GET_NEW_USERS = 'GET_NEW_USERS';
export const DELETE_USERS = 'DELETE_USERS';


export function getAllUsers(page = 1) {
    return dispatch=>{
        dispatch({
            type: `${GET_ALL_USERS}_LOADING`
        });
        fetch(`${window.location.origin}/api/users?page=${page}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(users=>dispatch({
                type: GET_ALL_USERS,
                payload: users
                })
            )
            .catch(err=>{
                dispatch({
                    type: `${GET_ALL_USERS}_ERROR`
                })
            })
    }
}

export function getNewUsers(group_name) {
    return dispatch=>{
        fetch(`${window.location.origin}/api/users/notingroup/${group_name}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(users=>dispatch({
                type: GET_NEW_USERS,
                payload: users
            }))
    }
}

export function searchUsers(pattern, crit) {
    return dispatch=>{
        fetch(`${window.location.origin}/api/users/search/?crit=${crit}&pattern=${pattern}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(users=>dispatch({
                type: SEARCH_USERS,
                payload: users
            }))
    }
}

export function getUsersCount() {
    return dispatch=>{
        fetch(`${window.location.origin}/api/users`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(usersCount=>dispatch({
                    type: GET_USERS_COUNT,
                    payload: +usersCount
                })
            )
    }
}

export function addUser(user) {
    return dispatch=>{
        dispatch({
            type: `${ADD_USER}_LOADING`
        });
        fetch(`${window.location.origin}/api/users`,{
            method: 'post',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                username:user.username,
                email:user.email,
                first_name:user.first_name,
                last_name:user.last_name
            })
        })
            .then(response=>response.json())
            .then(user=>dispatch({
                type: ADD_USER,
                payload: user
            }))
            .catch(err=>dispatch({
                type: `${ADD_USER}_ERROR`
            }))
    }
}

export function deleteUser(username) {
    return dispatch=>{
        fetch(`${window.location.origin}/api/users/${username}`, {
            method: 'delete'
        })
            .then(()=>dispatch({
                type: DELETE_USERS
            }))
    }
}

export function getUser(username) {
    return dispatch=>{
        dispatch({
            type: `${GET_USER}_LOADING`
        });
        fetch(`${window.location.origin}/api/users/${username}`, {
            method: 'get'
        })
            .then(response=>response.json())
            .then(user=>dispatch({
                type: GET_USER,
                payload: user
            }))
            .catch(err=>dispatch({
                type: `${GET_USER}_ERROR`
            }))
    }
}


export function editUser(username, user, index) {
    return dispatch=>{
        dispatch({
            type: `${EDIT_USER}_LOADING`
        });
        fetch(`${window.location.origin}/api/users/${username}`,{
            method: 'put',
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify(user)
        })
            .then(response=>response.json())
            .then(userInfo=>dispatch({
                type: EDIT_USER,
                payload: {
                    index,
                    user: userInfo
                }
            }))
            .catch(err=>dispatch({
                type: `${EDIT_USER}_ERROR`
            }))
    }
}



export function getUsername(username) {
    return {
        type: GET_USERNAME,
        payload: username
    }
}
