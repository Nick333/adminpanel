
export default function routingAction(method = 'push', url = '/'){
    return {
        type: 'ROUTING',
        payload:{
            method: method,
            nextUrl: url
        }
    }
}