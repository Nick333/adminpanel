export const OPEN_MODAL = 'OPEN_MODAL';

export function openModal(status) {
    return {
        type: OPEN_MODAL,
        payload: status
    }
}