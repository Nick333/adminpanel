import React from 'react';
import { Route, IndexRoute, IndexRedirect  } from 'react-router';
import App from './containers/App';
import GroupInfoContainer from './containers/GroupInfoContainer';
import GroupProfile from './containers/GroupProfile';
import GroupsContainer from './containers/GroupsContainer';
import GroupsList from './containers/GroupsList';
import GroupUsersContainer from './containers/GroupUsersContainer';
import UserProfile from './containers/UserProfile';
import UsersContainer from './containers/UsersContainer';
import NotFound from './containers/NotFound';
import UsersList from './containers/UsersList';

export const routes = (
    <Route path="/" component={App}>
        <IndexRedirect to="/users"/>
        <Route path="/users" component={UsersContainer}>
            <IndexRoute component={UsersList}/>
            <Route path="/users/:username" component={UserProfile}/>
        </Route>
        <Route path="/groups" component={GroupsContainer}>
            <IndexRoute component={GroupsList}/>
            <Route path="/groups/:group_name" component={GroupProfile}>
                <IndexRoute component={GroupInfoContainer}/>
                <Route path="/groups/:group_name/users" component={GroupUsersContainer}/>
            </Route>
        </Route>
        <Route path="/*" component={NotFound}/>
    </Route>
)