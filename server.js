var express = require('express');
const cors = require('cors');
var Groups= require('./server/routers/groups');
var Users = require('./server/routers/users');
const bodyParser= require('body-parser');
var app = express();
var dao = require('./server/dao/mongodb');
var morgan = require('morgan');

app.listen(3000, ()=> {
  console.log('listening port:3000');
});

dao.connect('localhost:27017/test', (err, db)=>{
  if(err){
    console.log('Error');
    process.exit();
  }else{
    dao.init();
    console.log('database connected');
  }
});

app.use(morgan('combined'));

app.use('/static', express.static(__dirname + '/dist'));

app.use(bodyParser.json());

app.use(cors());

app.use('/api/users', Users);

app.use('/api/groups', Groups);

app.get('*', (req, res)=>{
  res.sendFile(__dirname + '/dist/index.html')
});